// https://github.com/jean-emmanuel/open-stage-control/issues/266#event-1566718052

(function () {



    return {
        oscInFilter: function (data) {

            // Filter incomming osc messages

            var { address, args, host, port } = data

            if (host === 'midi' && address === '/control') {

                var channel = args[0].value,
                    cc = args[1].value,
                    value = args[2].value,
                    mappedValue

                if (cc === 1) {

                    if (value === 0) {
                        mappedValue = 0
                    } else if (value === 127) {
                        mappedValue = 127
                    } else {
                        mappedValue = 64
                    }
                    sendOsc({
                        address: data.address,
                        host: data.host,
                        port: data.port,
                        args: [
                            args[0], // same channel
                            { type: 'i', value: 91 }, // new cc
                            { type: 'i', value: mappedValue } // mapped value
                        ]
                    })

                }


            }

            // return data if you want the message to be processed
            // the original data hasn't changed so the widgets will receive it as is
            return { address, args, host, port }

        }
    }

})()