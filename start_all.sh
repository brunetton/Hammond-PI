#!/bin/bash

sudo hostapd /home/pi/conf/Hammond-PI/hostapd.conf >> /home/pi/logs/hostapd.log &
/home/pi/conf/Hammond-PI/start_jack >> /home/pi/logs/jack.log &
sleep 1

/home/pi/.nvm/versions/node/v8.9.0/bin/node /home/pi/dev/open-stage-control/app/ -n -m openstage:virtual -l /home/pi/conf/Hammond-PI/osc.json --custom-module /home/pi/conf/Hammond-PI/osc_module.js >> /home/pi/logs/osc.log &
a2jmidid --export-hw >> /home/pi/logs/a2jmidid.log &
/home/pi/dev/setBfree/src/setBfree -c /home/pi/conf/Hammond-PI/setbfree.cfg -p /home/pi/conf/Hammond-PI/setbfree_programs.pgm >> /home/pi/logs/setbfree.log &
python3 /home/pi/dev/jack-matchmaker/jackmatchmaker/ -p /home/pi/conf/Hammond-PI/jackmatchmaker.conf >> /home/pi/logs/jack-matchmaker.log 2>&1 &
